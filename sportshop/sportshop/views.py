from django.shortcuts import render
from django.views.generic import TemplateView


#Class based Views
class ContactView(TemplateView):
    template_name = 'contact.html'

    def get(self, request, *args, **kwargs):
       return super().get(request, *args, **kwargs)


class AboutView(TemplateView):
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        print(request.path)
        return super().get(request, *args, **kwargs)

#
# # Functional Views
# def contact(request):
#     return render(request, 'contact.html')
#
#
# def about(request):
#     return render(request, 'about.html')
