from django import forms
from .models import Articles, Feedback

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields =['content']