from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from .forms import FeedbackForm
import pdb

from .models import Articles
# functional Based views
# def home(request):
#     # Sample blog data
#     posts = Posts.objects.all()
#     posts = Posts.objects.order_by('-date_posted')
#
#     context = {'posts': posts}
#     return render(request, 'home.html', context)


# class based Views
class HomeView(ListView):
    model = Articles
    template_name = 'home.html'
    context_object_name = 'articles'
    ordering = ['name']


class ArticleFeedbackView(DetailView):
    model = Articles
    template_name = 'articles/article_feedback.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['feedback'] = self.object.feedbacks.all()
        return context

    def feedback(self, request, *args, **kwargs):
        pdb.set_trace()
        print(request.POST)
        self.object = self.get_object()
        feedback_form = FeedbackForm(request.POST)
        if feedback_form.is_valid():
            comment = feedback_form.save(commit=False)
            comment.author = request.user
            comment.post = self.object
            comment.save()
            return redirect('article_feedback', pk=self.object.id)
        context = self.get_context_data(object=self.object)
        context['feedback_form'] = feedback_form
        return self.render_to_response(context)
